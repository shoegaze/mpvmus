#!/bin/sh

MUSCOMMAND="env DRI_PRIME=0 /usr/bin/mpv --osc=no --save-position-on-quit --gapless-audio --no-video"
DLG=""
DLG_OVERRIDE=""
PARAM_DIR=""
PARAM_SHUFFLE=""
PARAM_WMCLASS=""
PARAM_TITLE=""
PARAM_EXTRA=""
MUSDIR=""

opt_help() {
	echo 'mpvmus - wrapper for mpv.'
	echo "Usage: $(basename "$0") [OPTIONS]"
	echo
	echo 'Options:'
	echo '  -h          Show this help text.'
	echo '  -o COMMAND  Override dialog program with COMMAND.'
	echo '  -d STRING   PARAM_DIR for dialog override.'
	echo '  -s STRING   PARAM_SHUFFLE for dialog override.'
	echo '  -e STRING   PARAM_EXTRA for dialog override.'
}

choose_dialog() {
	for prog in "kdialog" "zenity" "dialog"; do
		if which $prog > /dev/null 2>&1; then
			case $prog in
				"kdialog")
					DLG=$prog
					PARAM_DIR="--getexistingdirectory"
					PARAM_SHUFFLE="--yesno"
					PARAM_WMCLASS="--name mpvmus"
					PARAM_TITLE="--title mpvmus"
					break
					;;
				"zenity")
					DLG=$prog
					PARAM_DIR="--file-selection --directory"
					PARAM_SHUFFLE="--question --text"
					PARAM_WMCLASS="--name=mpvmus"
					PARAM_TITLE="--title=mpvmus"
					break
					;;
				"dialog")
					DLG=$prog
					PARAM_DIR="--stdout --clear --dselect ${HOME}/ 0 0"
					PARAM_SHUFFLE="--stdout --clear --yesno"
					PARAM_TITLE="--title mpvmus"
					PARAM_EXTRA="0 0"
					break
					;;
			esac
		fi
	done
}

while getopts ":ho:d:s:e:" arg; do
	case $arg in
		h)
			opt_help
			exit 0
			;;
		o)
			DLG_OVERRIDE="${OPTARG}"
			;;
		d)
			PARAM_DIR="${OPTARG}"
			;;
		s)
			PARAM_SHUFFLE="${OPTARG}"
			;;
		e)
			PARAM_EXTRA="${OPTARG}"
			;;
		:)
			echo "Missing argument for option -${OPTARG}"
			opt_help
			exit 1
			;;
		?)
			echo "Unknown option: -${OPTARG}"
			opt_help
			exit 1
			;;
	esac
done

if [ -z $DLG_OVERRIDE ]; then choose_dialog; else DLG=$DLG_OVERRIDE; fi
MUSDIR=$($DLG $PARAM_WMCLASS $PARAM_DIR)
if [ -z $MUSDIR ]; then exit 1; fi

if $DLG $PARAM_WMCLASS $PARAM_TITLE $PARAM_SHUFFLE "Enable shuffle?" $PARAM_EXTRA; then
	MUSCOMMAND="${MUSCOMMAND} --shuffle"
fi

notify-send -a "mpvmus" -i "mpv" -u low "mpvmus is now playing ${MUSDIR}"
$MUSCOMMAND --playlist="${MUSDIR}" &
